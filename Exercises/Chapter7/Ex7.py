# print the sentence on terminal
print "Mary had a little lamb."
# print the sentence and input the string 'snow' on the terminal
print "Its fleece was white as %s." % 'snow'
# print the sentece on the terminal
print "And everywhere that Mary went."
print "." * 10 # what'd that do? It print 10 '.'(dot) on a new line on the terminal

# setting the string for each variable from end1 until end12
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that comma at the end. try removing it to see what happens
# removing the comma at the end of 'end6' will make the program to print the next
# word in new line.
# if the comma stay, the next word will be printed next to the first word
print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 + end11 + end12