def bike_and_car(number_of_bikes, number_of_cars):
	print "Wow, you have %d bikes?" % number_of_bikes
	print "and you also have %d cars?" % number_of_cars
	print "Did you sell your hair to get money for all that?\n"

print "Way 1"
bike_and_car(30, 40)

print "Way 2"
no_bikes = 2
no_cars = 3

bike_and_car(no_bikes * 10, no_cars * 20)
