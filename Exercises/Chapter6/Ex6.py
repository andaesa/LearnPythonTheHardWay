# setting the string for x
x = "There are %d types of people." % 10
# setting the string for binary
binary = "binary"
# setting the string for do_not
do_not = "don't"
# setting the string for y
y = "Those who knows %s and those who %s." % (binary, do_not)

# print result of x and y on the terminal
print x
print y

# print another line for these two sentence
print "I said: %r." % x
print "I also said: '%s'." % y

# setting the boolean for hilarious
hilarious = False
# setting the string for joke_evaluation.'%r' is needed for conversion during string formatting 
joke_evaluation = "Isn't that joke so funny?! %r"

# print the sentence on the terminal
print joke_evaluation % hilarious

# setting the string for w and e
w = "This is the left side of..."
e = "a string with a right side."

# print the result of concatenation between w and e on the terminal
# adding w and e with + makes it longer since it is the concatenation between two strings.
print w + e

