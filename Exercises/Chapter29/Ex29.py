people = 20
cats = 30 
dogs = 15


if people < cats:
	print "Too many cats! The world is doomed!"

if people > cats:
	print "Not many cats! the world is saved!"

if people < dogs:
	print "The world is drooled on!"

if people > dogs:
	print "The world is dry!"


dogs += 5

if people >= dogs:
	print "People are greater than or equal to dogs."

if people <= dogs:
	print "People are less than or equal to dogs."

if people == dogs:
	print "People are dogs."


# the if-statement give the script a condition to run. The code under 'if' need
# to be indented to show that the code is in the specific if-statement'specific
# block. If it isn't indented, error will occur.

if people == dogs or cats == dogs:
	print "What happened to this world?"