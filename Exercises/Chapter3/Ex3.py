# this will print a sentence "I will now count my chickens:" on the terminal 
print "I will now count my chickens:"

# this will print the number of hens on the terminal
print "Hens", 25.0 + 30.0 / 6.0
# this will print the number of roosters on the terminal
print "Roosters", 100.0 - 25.0 * 3.0 % 4.0
# this will print a blank line
print ""

# this will print a sentence "Now I will count the eggs:" on the terminal
print "Now I will count the eggs:"
# this will print the number of eggs on the terminal
print 3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0
# this will print a blank line on the terminal
print ""

# this will print a sentence "Is it true that 3 + 2 < 5 - 7" on the terminal
print "Is it true that 3 + 2 < 5 - 7?"
# this will print "True" or "False" on the terminal
print 3.0 + 2.0 < 5.0 - 7.0
# this will print a blank line
print ""

# this will print the solution for 3 + 2 on the terminal
print "What is 3 + 2?", 3.0 + 2.0
# this will print the solution for 5 - 7 on the terminal
print "What is 5 - 7?", 5.0 - 7.0

# this will print a setence "Oh, that's why it's False" on the terminal
print "Oh, that's why it's False."
# this will print a blank line
print ""

# this will print a sentence "How about some more" on the terminal
print "How about some more."
# this will print a blank line
print ""

# this will print "True" or "False" based on given operation
print "Is it greater?", 5.0 > -2.0
# this will print "True" or "False" based on given operation
print "Is it greater or equal?", 5.0 >= -2.0
# this will print "True" or "False" based on given operation
print "Is it less or equal?", 5.0 <= -2.0
# this will print a blank line
print ""