# calculating price of the groceries
print "I am calculating price for the groceries"
print 2.80 + 3.40 + 2.30 - 10.50 * 1.50
print ""

# calculating the currency change between Yen and Ringgit with floating number
print "I am calculating the currency for Yen and Ringgit"
print "Current rate is  100 yen for Rm 4.05"
print "How much yen for Rm 423.37?", 423.37 / 4.05 * 100
print ""

# calculating the currency change between Yen and Ringgit with whole number
print "I am calculating the currency for Yen and Ringgit"
print "Current rate is 100 yen for Rm 4"
print "How much yen for Rm 423?", 423 / 4 * 100
print ""

# checking the difference for whole number and floating number
print "Checking the difference between whole number and floating number"
print "answer 1", 10.0 / 3.0
print "answer 2", 10 / 3
print ""
