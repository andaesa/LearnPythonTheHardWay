formatter = "%s %r %r %r"

print formatter % (1, 2, 3, 4)
print formatter % ("one", "two", "three", "four")
print formatter % (True, False, False, True)
print formatter % (formatter, formatter, formatter, formatter)
print formatter % (
	"I had this thing.",
	"That you couldn't type up right.",
	"But it didn't sing.",
	"So I said goodnight."
	)

# at the last line of output, one of the output uses both single-quotes
# and double quotes for individual pieces mainly because to differentiate
# between single-quotes and double-quotes 