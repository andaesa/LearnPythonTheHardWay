# import necessary module from package
from sys import argv
# assigning the argument variable
script, filename = argv
# opening the text file
txt = open(filename)
# print a sentence showing what file the user opened
# and print the content of the file on the terminal
print "Here's your file %r: " % filename
print txt.read()

# Close opened file
txt.close()

# print a sentence asking the user to enter the name 
# of the file by using built-in function 'raw_input'
####print "Type the filename again: "
####file_again = raw_input("> ")
# opening the file
####txt_again = open(file_again)
# print the content of the file on the terminal
####print txt_again.read()

# Close opened file
#txt_again.close()
