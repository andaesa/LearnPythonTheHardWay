tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = '''
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
'''

cat_stripe = "ugly"
cat_dot = "cute"

print "I have a cat but it's \"%s\"" % cat_stripe
print "I want that 100percent \'%r\'" % cat_dot
print ""
print tabby_cat
print persian_cat
print backslash_cat
print fat_cat


#while True:
	#for i in ["/","-","|","\\","|"]:
		#print "%s\r" % i,

