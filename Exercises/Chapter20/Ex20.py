# import necessary module from package
from sys import argv

# assigning variable for the arguments
script, input_file = argv

# defining the function for print_all
def print_all(f):
	print f.read()
# defining the function for rewind
def rewind(f):
	f.seek(0)
# defining the function for print_a_line
def print_a_line(line_count, f):
	print line_count, f.readline()
# assigning the value for current_file to open the input file
current_file = open(input_file)
# this will print whole contents of the file on the terminal
print "First let's print the whole file:\n"

print_all(current_file)
# this will rewind back the file to be read
print "Now let's rewind, kind of like a tape."

rewind(current_file)
# this will print the contents of the file line by line on the terminal
print "Let's print three lines:"

current_line = 1
print_a_line(current_line, current_file)
# + 1 here means to read the next line after current line 
#current_line = current_line + 1
current_line += 1
print_a_line(current_line, current_file)

#current_line = current_line + 1
current_line += 1
print_a_line(current_line, current_file)