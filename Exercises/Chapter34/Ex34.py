colors = ['red', 'blue', 'black', 'grey', 'yellow', 'green']

print "First color is %s." % colors[0]
print "Second color is %s." % colors[1]
print "Last color is %s." % colors[-1]