from sys import argv

script, first, second, third = argv

print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third


# if the arguments are fewer than three results in error as the 
# arguments is already set to certain number of variable in original script/code

confirm = raw_input("Did you enter the arguments? ")

print "Your answer is %s." % confirm
