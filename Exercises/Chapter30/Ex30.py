# assign value for each variables
people = 30
cars = 40
trucks = 15

# condition for cars > people and cars < people and cars == people
if cars > people:
	print "We should take the cars."
elif cars < people:
	print "We should not take the cars."
else:
	print "We can't decide."

# condition for trucks > cars and trucks < cars and trucks == cars
if trucks > cars:
	print "That's too many trucks."
elif trucks < cars:
	print "Maybe we could take the trucks."
else:
	print "We still can't decide."

# condition for people > trucks and people == trucks
if people > trucks:
	print "Alright, let's just take the trucks."
else:
	print "Fine, let's stay home then."

if cars > people or trucks < cars:
	print "What should we do?"

# elif and else function as is-statement that works for multiple condition.