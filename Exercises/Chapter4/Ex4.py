# setting the parameter / value for cars
cars = 100
# setting the parameter / value for space in car
space_in_a_car = 4.0
# setting the parameter / value for drivers
drivers = 30
# setting the parameter / value for passengers
passengers = 90
# setting the parameter / value for cars not driven
cars_not_driven = cars - drivers
# setting the parameter / value for cars driven
cars_driven = drivers
# setting the parameter / value for carpool capacity
carpool_capacity = cars_driven * space_in_a_car
# setting the parameter / value for average passenger per car
average_passenger_per_car = passengers / cars_driven

# print the result on the terminal
print ""
print "There are", cars, "cars available."
print "There are only", drivers, "drivers available."
print "There will be", cars_not_driven, "empty cars today."
print "We can transport", carpool_capacity, "people today."
print "We have", passengers, "to carpool today."
print "We need to put about", average_passenger_per_car, "in each car."
print ""